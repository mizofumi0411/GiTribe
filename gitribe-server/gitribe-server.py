# $ sqlite3 ./gitribe.db CREATE TABLE users ( id integer primary key, name text, password text, github text, role text, shell text);
from flask import Flask, jsonify, g, request
from flask_cors import CORS
from sqlite3 import dbapi2 as sqlite3
import json
from simple_aes_cipher import AESCipher, generate_secret_key

pass_phrase = "yjsnp1"
secret_key = generate_secret_key(pass_phrase)
# generate cipher
cipher = AESCipher(secret_key)

DATABASE = './gitribe.db'
app = Flask(__name__)
CORS(app)

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
        db.row_factory = sqlite3.Row
#        db.row_factory = dict_factory
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None: db.close()

def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv

def init_db():
    with app.app_context():
        db = get_db()
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

def sql_add_user(name, password, github, role, shell):
    sql = "INSERT INTO users (name, password, github, role, shell) VALUES('%s', '%s', '%s', '%s', '%s')" %(name, cipher.encrypt(password), github, role, shell)
    print sql
    db = get_db()
    db.execute(sql)
    res = db.commit()
    return res

def sql_delete_user(ID):
    sql = "delete from users where id = %d" %(int(ID))
    print sql
    db = get_db()
    db.execute(sql)
    res = db.commit()
    return res

def find_user(name=''):
    sql = "select * from users where name = '%s' limit 1" %(name)
    print sql
    db = get_db()
    rv = db.execute(sql)
    res = rv.fetchall()
    rv.close()
    return res[0]

def all_user():
    sql = "select * from users"
    print sql
    db = get_db()
    db.row_factory = dict_factory
    rv = db.execute(sql)
    res = rv.fetchall()
    rv.close()
    return res

@app.route('/')
def users():
    return jsonify(hello='world')

@app.route('/add',methods=['POST'])
def add_user():
    print sql_add_user(name=request.form['name'],password=request.form['password'], github=request.form['github'], role=request.form['role'], shell=request.form['shell'])
    return ''

@app.route('/users')
def get_all_users():
    res = all_user()
    return jsonify(res)

@app.route('/delete_user')
def delete_user():
    ID = request.args.get('id','')
    sql_delete_user(ID)
    return ''

@app.route('/find_user')
def find_user_by_name():
    name = request.args.get('name', '')
    user = find_user(name)
    print user
    return jsonify(id=user['id'],name=user['name'],password=user['password'], github=user['github'], role=user['role'], shell=user['shell'])

if __name__ == '__main__' : app.run(host='192.168.11.151',port=8000,debug=True)
