# Install

Ubuntu  
```
$ sudo apt install python-pip python-dev  
$ git clone https://bitbucket.org/mizofumi0411/gitribe/src/master/gitribe-server/  
$ cd gitribe/gitribe-server/  

$ pip install -r requirements.txt  
# or  
$ sudo pip install -r requirements.txt  
``` 

# Launch  
```
$ python gitribe-server.py  
```

# API Request Example  
```

# Get Users  
$ curl 'http://<ip>:<port>/users'  

# Add Users  
$ curl -X POST 'http://<ip>:<port>/add' -d name=mizofumi -d password=hogefuga -d github=mizofumi0411 -d role=default -d shell=bash  

# Delete User  
$ curl 'http://<ip>:<port>/delete_user?id=1'  

```
