$(function () {

    var api = 'https://gitribeapi.yjsnpi.club/';
    $(document).on("click", '#userdel', function(){
        var getVal = getUrlVars();
	if(!confirm('本当に"'+$(this).attr('username')+'"を削除してもよろしいですか？')){
	}else{
		deleteUser($(this).attr('userid'));
		location.href = "/?role="+getVal['role'];
	}
    });

    $('#reg_rolesend').click(function(){
        console.log($('#reg_rolename').val());
        location.href = "/?role="+$('#reg_rolename').val();
    });

    $('#reg_usersend').click(function(){
        var getVal = getUrlVars();
        var name = $('#reg_username').val();
        var password = $('#reg_password').val();
        var github = $('#reg_github').val();
        var shell = $('#reg_shell').val();
        registUser(name,password,github,getVal['role'],shell);

        location.href = "/?role="+getVal['role'];
    });

    function deleteUser(id){
        $.ajax({
            type: "GET",
            url: api+"delete_user?id="+id,
            dataType: 'text',
            success: function (data) {
            }
        });
    }

    function registUser(name,password,github,role,shell){
        $.ajax({
            type: "POST",
            url: api+"add",
            dataType: 'text',
            data: {
              "name": name,
              "password": password,
              "github": github,
              "role": role,
              "shell": shell
            },
            success: function (data) {
            }
        });
    }

    function init() {

        var getVal = getUrlVars();
        console.log(getVal['role']);

        if (getVal['role']) {
            $('#listtitle').text('Role: ' + getVal['role']);
            $('#regist_role').hide();
            $('#regist_user').show();
            $('#rolelist').hide();
            $('#userlist').show();
        } else {
            $('#listtitle').text('Role list');
            $('#regist_role').show();
            $('#regist_user').hide();
            $('#rolelist').show();
            $('#userlist').hide();
        }

        $.ajax({
            type: "GET",
            url: api+"users",
            dataType: 'json',
            data: "",
            success: function (data) {
                if (!getVal['role']) {
                    var roles = [];
                    for (i = 0; i < data.length; i++) {
                        roles.push(data[i]['role']);
                    }
                    roles = Array.from(new Set(roles))
                    for (i = 0; i < roles.length; i++) {
                        $('#rolelist').append(addRoleList(roles[i]));
                        console.log(roles[i]);
                    }
                    console.log(data);
                } else {
                    for (i = 0; i < data.length; i++) {
                        if (data[i]['role'] == getVal['role']) {
                            $('#userlist').append(addUserList(data[i]['id'], data[i]['name'], data[i]['github'], data[i]['shell']));
                        }
                    }

                }
            }
        });
    }

    function addUserList(id, name, github, shell) {
        return '\<tr id="user-' + id + '" >\
                <td>' + name + '</td>\
                <td>' + github + '</td>\
                <td>' + shell + '</td>\
                <td style="width: 10px"><a id="userdel" username="'+name+'" userid="'+id+'" class="indigo btn" style="" ><i class="material-icons left" style="margin-left: -10px;margin-right: -34px;">delete</i></a></td>\
                </tr>';
    }

    function addRoleList(role) {
        return '\<tr id="' + role + '" >\
                <td><a href="?role=' + role + '">' + role + '</a></td>\
                </tr>';
    }

    var getUrlVars = function () {
        var vars = {};
        var param = location.search.substring(1).split('&');
        for (var i = 0; i < param.length; i++) {
            var keySearch = param[i].search(/=/);
            var key = '';
            if (keySearch != -1) key = param[i].slice(0, keySearch);
            var val = param[i].slice(param[i].indexOf('=', 0) + 1);
            if (key != '') vars[key] = decodeURI(val);
        }
        return vars;
    }

    init();

});
