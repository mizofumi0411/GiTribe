#!/usr/bin/python

from crontab import CronTab
from datetime import datetime
import argparse
import os, sys
import requests
import commands
import platform
import traceback

serveraddr = '/root/.gitribe_server'
rolename = '/root/.gitrive_role'

from simple_aes_cipher import AESCipher, generate_secret_key

pass_phrase = "yjsnp1"
secret_key = generate_secret_key(pass_phrase)

# generate cipher
cipher = AESCipher(secret_key)

def main():
    if not os.geteuid() == 0:
        sys.exit("\nOnly root can run this script\n")

    cronscriptname = 'gitribe'
    cron = CronTab(tabfile='/etc/crontab', user='root')
    parser = argparse.ArgumentParser(description='')

    parser.add_argument(
        '--set',
        type=str,
        nargs='*',
        help='--set server <url>, crontab'
    )

    parser.add_argument(
        '--unset',
        type=str,
        nargs='*',
        help='--unset server <url>, crontab'
    )

    parser.add_argument(
        'setting',
        nargs='*',
        help='show setting value'
    )

    args = parser.parse_args()
    if args.setting:
        cronstatus = "Off"
        print 'API Server: ' + getAPIServerURL()
        print 'Role: '+ getRoleName()
        for job in cron:
            if cronscriptname in str(job):
                cronstatus = "On"
        print 'Cron: ' + cronstatus

    elif args.set:
        if args.set[0] == 'server':
            file = open(serveraddr, 'w')
            file.write(args.set[1])
            print(args.set[1])

        elif args.set[0] == 'role':
            file = open(rolename, 'w')
            file.write(args.set[1])
            print(args.set[1])

        elif args.set[0] == 'crontab':
            # unsetCrontab()
            # setCrontab()
            job = cron.new(command='root /usr/bin/gitribe')
            job.minute.every(1)
            cron.write()
            print 'SET CRONTAB!'

    elif args.unset:
        if args.unset[0] == 'server':
            os.remove(serveraddr)
            print('UNSET SERVER!')

        elif args.unset[0] == 'role':
            os.remove(rolename)
            print('UNSET ROLE!')

        elif args.unset[0] == 'crontab':
            # unsetCrontab()
            for job in cron:
                if cronscriptname in str(job):
                    cron.remove(job)
                    cron.write()
            print('UNSET CRONTAB!')
    else:
        sync()

def unsetCrontab():
    commands.getstatusoutput("sudo crontab -u root -l | sed -e '/gitribe/d' | sudo crontab -u root -")
    commands.getstatusoutput("sudo /etc/init.d/cron restart")

def setCrontab():
    commands.getstatusoutput("(sudo crontab -u root -l ; echo '*/1 * * * * sudo /usr/bin/gitribe') | sudo crontab -u root -")
    commands.getstatusoutput("sudo /etc/init.d/cron restart")

def printError(message):
    print bcolors.FAIL+message+bcolors.ENDC
    commands.getstatusoutput('echo '+message+' >> /tmp/gitribe.log')

def getAPIServerURL():
    # Server Address Check
    if not os.path.exists(serveraddr):
        printError("Error: Server Address not found...")
        exit(1)
    file = open(serveraddr, 'r')
    url = str(file.read())
    file.close()
    return url

def getRoleName():
    # Server Address Check
    if not os.path.exists(rolename):
        return 'default'
    file = open(rolename, 'r')
    url = str(file.read())
    file.close()
    return url

def certfileDownloadFromGithub(username):
    r = requests.get("https://github.com/" + username + ".keys")
    if r.status_code == 200:
        return r
    else:
        raise Exception("Failed '"+username+"' public key download from GitHub")


def setSSHCertificate(homedir, username, github, shell):
    if not os.path.isdir(homedir + "/.ssh"):
        os.mkdir(homedir + "/.ssh")
    try:
        pub = certfileDownloadFromGithub(github)
        with open(homedir+"/.ssh/authorized_keys", 'wb') as saveFile:
            saveFile.write(pub.content)
        commands.getstatusoutput('sudo chown ' + username + ':' + username + ' ' + homedir + '/.ssh/')
        commands.getstatusoutput('sudo chown ' + username + ':' + username + ' ' + homedir + '/.ssh/authorized_keys')
        commands.getstatusoutput('sudo chmod go-w ' + homedir + '/.ssh/')
        commands.getstatusoutput('sudo chmod go-w ' + homedir + '/.ssh/authorized_keys')

    except Exception as e:
        raise e

def setDefaultShell(shell, username):
    setSudoPerm(username)
    commands.getstatusoutput("sudo usermod --shell /bin/" + shell + " " + username)

def setDefaultPass(username,password):
    setSudoPerm(username)
    commands.getstatusoutput('echo "'+username+':'+password+'" | sudo chpasswd')
    return

def setSudoPerm(username):
    if platform.dist()[0] == 'centos':
        commands.getstatusoutput('sudo usermod -aG wheel '+username)
    else:
        commands.getstatusoutput("sudo usermod -G sudo " + username)

def userExists(username):
    if commands.getstatusoutput('getent passwd ' + username)[0] == 0:
        return True
    return False

def makeHomeDir(username):
    if commands.getstatusoutput('useradd -m '+username)[0] == 0:
        commands.getstatusoutput("sudo usermod -G sudo " + username)
    else:
        raise Exception("Failed create "+username+"'s homedir...")

def sync():
    # API Request
    r = requests.get(getAPIServerURL())
    if r.status_code == 200:
        j = r.json()

        print bcolors.OKBLUE + "                     ExecTime: " + bcolors.ENDC + datetime.now().strftime("%Y/%m/%d %H:%M:%S")
        print bcolors.HEADER + '{}{}{}{}{}'.format('Username', ' ' * (15 - len('')),'GitHub' ,' ' * (15 - len('')), 'Result') + bcolors.ENDC
        ok = bcolors.OKGREEN + "OK" + bcolors.ENDC
        ng = bcolors.FAIL + "NG" + bcolors.ENDC

        for user in j:
            if user['role'] != getRoleName():
                continue
            result = ""
            username = user['name']
            password = cipher.decrypt(user['password'])
            github = user['github']
            shell = user['shell']

            # User Exists Check
            if not userExists(username):

                # Create User
                try:
                    makeHomeDir(username)

                except Exception as e:
                    printError(e.args[0])
                    result = ng

            homedir = commands.getoutput("/bin/echo ~" + username)
            if "/" in homedir:
                try:
                    setSSHCertificate(homedir, username, github, shell)
                    result = ok
                except Exception as e:
                    printError(e.args[0])
                    result = ng
                # print 'HOMEDIR'

            setDefaultShell(shell,username)
            setDefaultPass(username, password)
            print '{}{}{}{}{}'.format(username, ' ' * (23 - len(username)), github,' ' * (21 - len(github)), '[ ' + result + ' ]')

        # serverUsers = []
        #
        # for x in os.listdir('/home/'):
        #     if os.path.isdir('/home/' + x):
        #         serverUsers.append(x)
        #
        # for serverUser in serverUsers:
        #     for user in j:
        #         if user['name'] == serverUser:
        #             try:
        #                 serverUsers.remove(serverUser)
        #             except ValueError:
        #                 pass
        #
        # for serverUser in serverUsers:
        #     if os.path.exists('/home/' + serverUser + '/.ssh/authorized_keys'):
        #         commands.getstatusoutput('sudo rm /home/'+serverUser+'/.ssh/authorized_keys')

    else:
        printError("Error: API Request failed...")
        exit(1)


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


if __name__ == '__main__':
    main()
