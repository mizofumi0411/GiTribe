# Install
Ubuntu
```
$ sudo apt install python-pip python-dev
$ git clone https://bitbucket.org/mizofumi0411/gitribe/
$ cd gitribe/gitribe-client

$ sudo -H pip install -r requirements.txt

$ sudo mv ./gitribe.py /usr/bin/gitribe
$ sudo chmod a+x /usr/bin/gitribe

# ReLogin, command check
$ sudo gitribe settings
```

# Usage

## Setting
```
# set Server
$ sudo gitribe --set server <GiTribeServerURL>

# set Role
$ sudo gitribe --set role <rolename>

# Crontab On (every 1min
$ sudo gitribe --set crontab

# Check
$ sudo gitribe settings
```

## Execute
```
$ sudo gitribe
```
